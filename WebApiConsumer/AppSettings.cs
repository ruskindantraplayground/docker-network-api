using System;

namespace WebApiConsumer
{
    public class AppSettings
    {
        public Uri ApiUrl { get; set; }
    }
}