using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace WebApiConsumer
{
    internal class App : IHostedService, IDisposable
    {
        [NotNull] private readonly ILogger<App> _logger;
        [NotNull] private readonly IOptions<AppSettings> _appSettings;

        public App(ILogger<App> logger, [NotNull] IOptions<AppSettings> appSettings)
        {
            _logger = logger;
            _appSettings = appSettings;
            
            _logger.LogWarning($"Application initialized with setting <{appSettings.Value.ApiUrl}>");

            TestLogging();
        }

        private void TestLogging()
        {
            var timer = new Timer(state =>
            {
                _logger.LogInformation($"Timer called at <{DateTime.UtcNow}>");
                _logger.LogInformation($"v7");
                //_logger.LogError($"This is an error message at <{DateTime.UtcNow}>");

                HttpClient client = new HttpClient {BaseAddress = _appSettings.Value.ApiUrl};
                
                _logger.LogInformation($"HttpClient set to base address of: {client.BaseAddress}");
                
                HttpResponseMessage response = client.GetAsync($"weatherforecast").Result;
                _logger.LogInformation($"Status code is: {response.StatusCode}");
                response.EnsureSuccessStatusCode();
                string responseBody = response.Content.ReadAsStringAsync().Result;
            
                _logger.LogInformation(responseBody);
                
            }, null, TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(2));
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Starting.");
            return Task.CompletedTask;
        }
        
        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Stopping.");
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _logger.LogInformation("Disposing.");
        }
    }
}